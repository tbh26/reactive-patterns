import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'browser-event-experiments',
    templateUrl: './browser-event-experiments.component.html',
    styleUrls: ['./browser-event-experiments.component.css']
})
export class BrowserEventExperimentsComponent implements OnInit {

    hoverSection: HTMLElement;
    mouseMoveHandlerSubscribed = null;

    constructor() {
    }

    ngOnInit() {

        this.hoverSection = document.getElementById('hover');

        this.hoverSection.addEventListener('mousemove', myMouseMoveHandler);

        this.mouseMoveHandlerSubscribed = true;

        this.hoverSection.addEventListener('click', myMouseClickHandler);

        // this.hoverSection.onmousemove = (event) => {
        //     console.info('---- on mouse move, event:', event);
        // };

    }

    toggleMouseMoveHandlerSubscription () {
        console.info('-- toggle mouse move handler ' + getStamp());

        if ( this.mouseMoveHandlerSubscribed ) {
            this.hoverSection.removeEventListener('mousemove', myMouseMoveHandler);
            this.mouseMoveHandlerSubscribed = false;
        } else {
            this.hoverSection.addEventListener('mousemove', myMouseMoveHandler);
            this.mouseMoveHandlerSubscribed = true;
        }
    }

}

function myMouseMoveHandler ( event ) {
    console.info(`-- mouse move, stamp: ${getStamp()}, event:`, event);
}

function myMouseClickHandler ( event ) {
    console.info(`-- mouse click event:`, event);
}

function getStamp () {
    const stamp = new Date().getTime();
    return stamp;
}
